<?php
$create_url = array('module' => $module);
$messages = array();

$save_fields = array('route_id', 'day', 'start', 'end', 'agency');
$table = 'schedules';
$id_field = 'schedule_id';
if (isset($_POST['delete'])) {
    $del_id = realEscape($_POST['delete']);
    if (query("DELETE FROM $table WHERE $id_field = '$del_id'")) {
        $messages[] =  'success';
    }
    else {
        $messages[] =  'error';
    }
}
$cities = simpleSelect("SELECT * FROM cities");
$cities = array_combine(array_column($cities, 'city_id'), array_column($cities, 'city_name'));
$routes_temp = simpleSelect("SELECT * FROM routes");
$routes = array();
foreach ($routes_temp as $key => $value) {
    $value['text'] = $cities[$value['from_city']] . ' -> ' .  $cities[$value['to_city']];
    $routes[$value['route_id']] = $value;
}
if (isset($_POST['save'])) {
    $id = (int)($_GET['edit'] ?? 0);
    $save_ = array();
    $_POST['status'] = $_POST['status'] ?? 0;
    foreach ($_POST as $key => $value) {
        if (in_array($key, $save_fields)) {
            $save_[$key] = realEscape($value);
        }
    }
    $where = '';
    if (!empty($id)) {
        $where = "$id_field = '$id'";
    }

    if (insertOrUpdate($save_, $table, $where)) {
        $messages[] =  'success';
    }
    else {
        $messages[] =  'error';
    }
}




$data = simpleSelect("SELECT * FROM $table");


include_once 'head.php';
echo '<div style="height:100px">&nbsp;</div>';
if (!empty($messages)) {
    echo '<div class="topmg">';
    foreach ($messages as $key => $value) {
        echo '<div class="alert alert-success">' . $value . '</div>';
    }
    echo '</div>';
}
if (isset($_GET['edit']) || isset($_GET['new'])) { 
    $edit = (int)($_GET['edit'] ?? 0);
    $data_to_form = current(simpleSelect("SELECT * FROM $table WHERE $id_field = '$edit' "));
    ?>
    <div class="container topmg">
        <div class="modal-xs mt-3">
            <h2 class="text-center">Update</h2>
            <form action="" method="post">

                <div class="form-group">
                    <label for="route_id">From city</label>
                    <select name="route_id" id="route_id" class="form-control">
                        <?php
                            foreach ($routes as $route_id => $route) {
                                $selected = '';
                                if ($route_id == ($data_to_form['route_id'] ?? '')) {
                                    $selected = 'selected';
                                }
                                echo '<option value="' . $route_id . '" ' . $selected . '>' . $route['text'] . '</option>';
                            }
                        ?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="day">Day</label>
                    <select name="day" id="day" class="form-control">
                        <?php
                            foreach ($days as $day) {
                                $selected = '';
                                if ($day == ($data_to_form['day'] ?? '')) {
                                    $selected = 'selected';
                                }
                                echo '<option value="' . $day . '" ' . $selected . '>' . $day . '</option>';
                            }
                        ?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="start">Start</label>
                    <input type="time" class="form-control" id="start" placeholder="Enter start time" name="start" required
                           value="<?php echo htmlspecialchars($data_to_form['start'] ??  '') ?>">
                </div>
                <div class="form-group">
                    <label for="end">End</label>
                    <input type="time" class="form-control" id="end" placeholder="Enter end time" name="end" required
                           value="<?php echo htmlspecialchars($data_to_form['end'] ??  '') ?>">
                </div>
                <div class="form-group">
                    <label for="agency">Agency</label>
                    <input type="text" class="form-control" id="agency" placeholder="Enter agency" name="agency" required
                           value="<?php echo htmlspecialchars($data_to_form['agency'] ??  '') ?>">
                </div>
                <button type="submit" name="save" value="save" class="btn btn-success">Save</button>
            </form>
        </div>

    </div>
<?php 


} ?>
    <form action="" method="post" class="mt-5">
        <h2 class="text-center">Schedules</h2>
        <div class="table-responsive">
            <table class="table table-hovertable-bordered">
                <tr>
                    <th>#</th>
                    <th>Route</th>
                    <th>Day</th>
                    <th>Start</th>
                    <th>End</th>
                    <th>Agency</th>
                    <th>Edit</th>
                    <th>Delete</th>

                </tr>
                <?php
                foreach ($data as $value) {
                    echo '<tr>';
                    echo '<td>' . htmlspecialchars($value[$id_field]) . '</td>';
                    echo '<td>' . htmlspecialchars($routes[$value['route_id']]['text']) . '</td>';
                    echo '<td>' . htmlspecialchars($value['day']) . '</td>';
                    echo '<td>' . htmlspecialchars($value['start']) . '</td>';
                    echo '<td>' . htmlspecialchars($value['end']) . '</td>';
                    echo '<td>' . htmlspecialchars($value['agency']) . '</td>';

                    echo '<td><a class="btn btn-info" href="?' . http_build_query($create_url) . '&edit=' . $value[$id_field] . '">Edit</a></td>';
                    echo '<td><button type="submit" class="btn btn-dark" name="delete" value="' . $value[$id_field] . '">Delete</button></td>';
                    echo '</tr>';
                }
                ?>
            </table>
            <div class="text-center">
                <a class="btn btn-info"  href="?<?php echo http_build_query($create_url) . '&new';?>">New</a>
                
            </div>
        </div>

    </form>

<?php

include_once 'footer.php';

?>