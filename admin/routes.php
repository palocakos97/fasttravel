<?php
$create_url = array('module' => $module);
$messages = array();

$save_fields = array('from_city', 'to_city');
$table = 'routes';
$id_field = 'route_id';
if (isset($_POST['delete'])) {
    $del_id = realEscape($_POST['delete']);
    if (query("DELETE FROM $table WHERE $id_field = '$del_id'")) {
        $messages[] =  'success';
    }
    else {
        $messages[] =  'error';
    }
}
$cities = simpleSelect("SELECT * FROM cities");
$cities = array_combine(array_column($cities, 'city_id'), array_column($cities, 'city_name'));
if (isset($_POST['save'])) {
    $id = (int)($_GET['edit'] ?? 0);
    $save_ = array();
    $_POST['status'] = $_POST['status'] ?? 0;
    foreach ($_POST as $key => $value) {
        if (in_array($key, $save_fields)) {
            $save_[$key] = realEscape($value);
        }
    }
    $where = '';
    if (!empty($id)) {
        $where = "$id_field = '$id'";
    }

    if (insertOrUpdate($save_, $table, $where)) {
        $messages[] =  'success';
    }
    else {
        $messages[] =  'error';
    }
}




$data = simpleSelect("SELECT $id_field, from_city, to_city FROM $table");


include_once 'head.php';
echo '<div style="height:100px">&nbsp;</div>';
if (!empty($messages)) {
    echo '<div class="topmg">';
    foreach ($messages as $key => $value) {
        echo '<div class="alert alert-success">' . $value . '</div>';
    }
    echo '</div>';
}
if (isset($_GET['edit']) || isset($_GET['new'])) { 
    $edit = (int)($_GET['edit'] ?? 0);
    $data_to_form = current(simpleSelect("SELECT * FROM $table WHERE $id_field = '$edit' "));
    ?>
    <div class="container topmg">
        <div class="modal-xs mt-3">
            <h2 class="text-center">Update</h2>
            <form action="" method="post">

                <div class="form-group">
                    <label for="from_city">From city</label>
                    <select name="from_city" id="from_city" class="form-control">
                        <?php
                            foreach ($cities as $city_id => $city_name) {
                                $selected = '';
                                if ($city_id == ($data_to_form['from_city'] ?? '')) {
                                    $selected = 'selected';
                                }
                                echo '<option value="' . $city_id . '" ' . $selected . '>' . $city_name . '</option>';
                            }
                        ?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="to_city">To city</label>
                    <select name="to_city" id="to_city" class="form-control">
                        <?php
                            foreach ($cities as $city_id => $city_name) {
                                $selected = '';
                                if ($city_id == ($data_to_form['to_city'] ?? '')) {
                                    $selected = 'selected';
                                }
                                echo '<option value="' . $city_id . '" ' . $selected . '>' . $city_name . '</option>';
                            }
                        ?>
                    </select>
                </div>
                <button type="submit" name="save" value="save" class="btn btn-success">Save</button>
            </form>
        </div>

    </div>
<?php 


} ?>
    <form action="" method="post" class="mt-5">
        <h2 class="text-center">Routes</h2>
        <div class="table-responsive">
            <table class="table table-hovertable-bordered">
                <tr>
                    <th>#</th>
                    <th>From city</th>
                    <th>To city</th>
                    <th>Edit</th>
                    <th>Delete</th>

                </tr>
                <?php
                foreach ($data as $value) {
                    echo '<tr>';
                    echo '<td>' . htmlspecialchars($value[$id_field]) . '</td>';
                    echo '<td>' . htmlspecialchars($cities[$value['from_city']]) . '</td>';
                    echo '<td>' . htmlspecialchars($cities[$value['to_city']]) . '</td>';

                    echo '<td><a class="btn btn-info" href="?' . http_build_query($create_url) . '&edit=' . $value[$id_field] . '">Edit</a></td>';
                    echo '<td><button type="submit" class="btn btn-dark" name="delete" value="' . $value[$id_field] . '">Delete</button></td>';
                    echo '</tr>';
                }
                ?>
            </table>
            <div class="text-center">
                <a class="btn btn-info"  href="?<?php echo http_build_query($create_url) . '&new';?>">New</a>
                
            </div>
        </div>

    </form>

<?php

include_once 'footer.php';

?>