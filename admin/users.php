<?php
$create_url = array('module' => $module);
$messages = array();

$save_fields = array('name', 'email', 'status');
$table = 'users';

if (isset($_POST['delete'])) {
    $del_id = realEscape($_POST['delete']);
    if (query("DELETE FROM $table WHERE user_id = '$del_id'")) {
        $messages[] =  'success';
    }
    else {
        $messages[] =  'error';
    }
}


if (isset($_POST['save'])) {
    $id = (int)($_GET['edit'] ?? 0);
    $save_ = array();
    $_POST['status'] = $_POST['status'] ?? 0;
    foreach ($_POST as $key => $value) {
        if (in_array($key, $save_fields)) {
            $save_[$key] = realEscape($value);
        }
    }
    $where = '';
    if (!empty($id)) {
        $where = "user_id = '$id'";
    }
    else {
        $save_['password'] = sha1(SALT . ($_POST['password'] ?? '')); 
    }

    if (insertOrUpdate($save_, $table, $where)) {
        $messages[] =  'success';
    }
    else {
        $messages[] =  'error';
    }
}




$data = simpleSelect("SELECT user_id, name, email, status FROM $table");


include_once 'head.php';
echo '<div style="height:100px">&nbsp;</div>';
if (!empty($messages)) {
    echo '<div class="topmg">';
    foreach ($messages as $key => $value) {
        echo '<div class="alert alert-success">' . $value . '</div>';
    }
    echo '</div>';
}
if (isset($_GET['edit']) || isset($_GET['new'])) { 
    $edit = (int)($_GET['edit'] ?? 0);
    $data_to_form = current(simpleSelect("SELECT * FROM $table WHERE user_id = '$edit' "));



    ?>
    <div class="container topmg">
        <div class="modal-xs mt-3">
            <h2 class="text-center">Update</h2>
            <form action="" method="post">
                <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" class="form-control" id="name" placeholder="Enter name" name="name" required
                           value="<?php echo htmlspecialchars($data_to_form['name'] ??  '') ?>">
                </div>

                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email" class="form-control" id="email" placeholder="Enter email" name="email" required
                           value="<?php echo htmlspecialchars($data_to_form['email'] ??  '') ?>">
                </div>
                <?php

                if (empty($data_to_form)) {
                    echo '<div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" class="form-control" id="password" placeholder="Password" name="password" required>
                        </div>';
                }
                ?>


                <div class="form-group">
                    <label for="email">Admin</label>
                    <input type="checkbox" name="status" value="1" <?php echo (!empty($data_to_form['status']) ? 'checked' : '');?>>
                </div>
                <button type="submit" name="save" value="save" class="btn btn-success">Save</button>
            </form>
        </div>

    </div>
<?php 


} ?>
    <form action="" method="post" class="mt-5">
        <h2 class="text-center">Users</h2>
        <div class="table-responsive">
            <table class="table table-hovertable-bordered">
                <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>E-mail</th>
                    <th>Status</th>
                    <th>Edit</th>
                    <th>Delete</th>
                </tr>
                <?php
                foreach ($data as $value) {
                    echo '<tr>';
                    foreach ($value as $key => $item) {
                        echo '<td>' . htmlspecialchars($item) . '</td>';
                    }
                    echo '<td><a class="btn btn-info" href="?' . http_build_query($create_url) . '&edit=' . $value['user_id'] . '">Edit</a></td>';
                    echo '<td><button type="submit" class="btn btn-dark" name="delete" value="' . $value['user_id'] . '">Delete</button></td>';
                    echo '</tr>';
                }
                ?>
            </table>
            <div class="text-center">
                <a class="btn btn-info" href="?new">New</a>
                
            </div>
        </div>

    </form>

<?php

include_once 'footer.php';

?>