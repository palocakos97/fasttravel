<?php
if (file_exists('template/head.php')){
    require_once 'template/head.php';
}

if ($permission){
	echo '<h1 class="text-center m-3">404</h1>';
	echo '<h2 class="text-center text-capitalize">not found</h2>';
}
else {
	echo '<h1 class="text-center ">403</h1>';
	echo '<h2 class="text-center text-capitalize">Permission denied</h2>';
}

if (file_exists('template/footer.php')){
    require_once 'template/footer.php';
}
?>