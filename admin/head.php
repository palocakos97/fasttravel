<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>Admin</title>
  <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" >
<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="../css/admin.css">
<link rel="shortcut icon" href="../icon.ico"/>

</head>
<body>
<?php if ($permission) { ?>
    <!--TOP NAVBAR-->
    <div class="d-none d-md-block">
        <nav class="navbar navbar-expand-md navbar-dark bg-dark box-shadow fixed-top">
            <a class="navbar-brand" href="../">Fasttravel</a>
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="index.php?module=users">Users</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="index.php?module=cities">Cities</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="index.php?module=routes">Routes</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="index.php?module=schedules">Schedules</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link login-btn" href="#"><?php echo $_SESSION['user']['email']?></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link login-btn" href="../logout.php">Logout</a>
                </li>
            </ul>
        </nav>
    </div>


<?php } ?>