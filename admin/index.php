<?php
session_start();
require_once '../db_config.php';
require_once "../functions.php";

$module = 'users';
$permission = true;

if (!isset($_SESSION['user']['status']) || $_SESSION['user']['status'] != 1) {
    $module = 'error';
    $permission = false;
}

if (isset($_GET['module']) && !empty($_GET['module']) && $permission) {
    $module = $_GET['module'];
}
if (!file_exists($module . '.php') && $permission) {
    $module = 'error';
}
include_once $module . '.php';
?>
