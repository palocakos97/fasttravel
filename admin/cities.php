<?php
$create_url = array('module' => $module);
$messages = array();

$save_fields = array('city_name');
$table = 'cities';
$id_field = 'city_id';
if (isset($_POST['delete'])) {
    $del_id = realEscape($_POST['delete']);
    if (query("DELETE FROM $table WHERE $id_field = '$del_id'")) {
        $messages[] =  'success';
    }
    else {
        $messages[] =  'error';
    }
}


if (isset($_POST['save'])) {
    $id = (int)($_GET['edit'] ?? 0);
    $save_ = array();
    $_POST['status'] = $_POST['status'] ?? 0;
    foreach ($_POST as $key => $value) {
        if (in_array($key, $save_fields)) {
            $save_[$key] = realEscape($value);
        }
    }
    $where = '';
    if (!empty($id)) {
        $where = "$id_field = '$id'";
    }

    if (insertOrUpdate($save_, $table, $where)) {
        $messages[] =  'success';
    }
    else {
        $messages[] =  'error';
    }
}




$data = simpleSelect("SELECT $id_field, city_name FROM $table");


include_once 'head.php';
echo '<div style="height:100px">&nbsp;</div>';
if (!empty($messages)) {
    echo '<div class="topmg">';
    foreach ($messages as $key => $value) {
        echo '<div class="alert alert-success">' . $value . '</div>';
    }
    echo '</div>';
}
if (isset($_GET['edit']) || isset($_GET['new'])) { 
    $edit = (int)($_GET['edit'] ?? 0);
    $data_to_form = current(simpleSelect("SELECT * FROM $table WHERE $id_field = '$edit' "));
    ?>
    <div class="container topmg">
        <div class="modal-xs mt-3">
            <h2 class="text-center">Update</h2>
            <form action="" method="post">
                <div class="form-group">
                    <label for="name">City name</label>
                    <input type="text" class="form-control" id="city_name" placeholder="Enter name" name="city_name" required
                           value="<?php echo htmlspecialchars($data_to_form['city_name'] ??  '') ?>">
                </div>

                <button type="submit" name="save" value="save" class="btn btn-success">Save</button>
            </form>
        </div>

    </div>
<?php 


} ?>
    <form action="" method="post" class="mt-5">
        <h2 class="text-center">Cities</h2>
        <div class="table-responsive">
            <table class="table table-hovertable-bordered">
                <tr>
                    <th>#</th>
                    <th>City name</th>
                    <th>Edit</th>
                    <th>Delete</th>

                </tr>
                <?php
                foreach ($data as $value) {
                    echo '<tr>';
                    foreach ($value as $key => $item) {
                        echo '<td>' . htmlspecialchars($item) . '</td>';
                    }
                    echo '<td><a class="btn btn-info" href="?' . http_build_query($create_url) . '&edit=' . $value[$id_field] . '">Edit</a></td>';
                    echo '<td><button type="submit" class="btn btn-dark" name="delete" value="' . $value[$id_field] . '">Delete</button></td>';
                    echo '</tr>';
                }
                ?>
            </table>
            <div class="text-center">
                <a class="btn btn-info"  href="?<?php echo http_build_query($create_url) . '&new';?>">New</a>
                
            </div>
        </div>

    </form>

<?php

include_once 'footer.php';

?>