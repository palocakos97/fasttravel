<?php
include_once 'head.php';
if (isset($_POST['send_login'])) {
    $msg = checkRequire(['email', 'password'], false);
    if (empty($msg['error'])) {
        $exceptions = ['send_login'];
        $data = dataFiltering($_POST, $exceptions);
        $psw = sha1(SALT . $data['password']);
        $user_check = paramSelect(['*'], 'users', "password = '{$psw}' AND email = '{$data['email']}'");
        if (!empty($user_check)) {
            $_SESSION['user'] = current($user_check);
            session_regenerate_id();
            header('location:index.php');
        }
        else {
            $msg['error'][] = 'Incorrect email or password';
        }
    }

}

?>
<body>
  <div class="container">
    <div class="row">
      <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
        <div class="card card-signin my-5">
          <div class="card-body">
            <h5 class="card-title text-center">Sign In</h5>
            <?php

            if (!empty($msg['error'])) {
                foreach ($msg['error'] as $mes){
                    echo '<div class="alert alert-danger text-center mx-auto">' . $mes . '</div>';
                }
            }
            if (isset($_GET['reg_success'])) {
                echo '<div class="alert alert-success text-center mx-auto">Registration successful, please log in!</div>';
            }
            ?>
            <form class="form-signin" method="POST" action="login.php">
              <div class="form-label-group">
                <input name="email" type="email" id="inputEmail" class="form-control" placeholder="Email address" required autofocus><br/>
              </div>

              <div class="form-label-group">
                <input name="password" type="password" id="inputPassword" class="form-control" placeholder="Password" required><br/>
              </div>


              <button class="btn btn-lg btn-primary btn-block text-uppercase" type="submit" name="send_login">Sign in</button>
              <hr class="my-4">
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</body>

<?php include_once 'foot.php'; ?>