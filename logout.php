<?php
session_start();
unset($_SESSION['user']);
session_regenerate_id();
header('location: index.php');
exit;