var autocompleteSelect;
$(function () {
    var options = {
        max_value: 5,
        step_size: 1,
        initial_value: 0,
        selected_symbol_type: 'utf8_star', // Must be a key from symbols
        cursor: 'default',
        readonly: false,
        change_once: true, // Determines if the rating can only be set once
        ajax_method: 'POST',
        url: 'save_rate.php',
    }

    $('.js-city-autocomplete').autocomplete({
        source: cities,
        focus: function( event, ui ) {
            $(this).val(ui.item.label);
            return false;
        },
        select: function (event, ui) {
            return autocompleteSelect(event, ui);
        },
    });

    autocompleteSelect = function  (event, ui) {
        var city_id = ui.item.value;
        var timetable = $('.js-timetable');
        if (timetable.length == 0) {
            window.location = 'timetable.php?city_id=' + city_id;
        }
        else {
            $.ajax({
              method: 'POST',
              url: 'ajax_timetable.php',
              data: { 
                city_id: city_id
              }
            }) 
            .done(function(result) {
                timetable.html(result);
                $(".rating").rate(options);
                $(".rating").on("change", function(ev, data){
                    $(this).rate("setAdditionalData", {route_id: $(this).data('route-id')});
                });

            });
        }
        $(this).val(ui.item.label);

        return false;

    }


});