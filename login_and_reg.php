<!--LOGIN-->
<div class=" block-vertical-center" id="login-bg">
    <div class="login-container">
        <div class="row">
            <div class="col-md-6  order-2 order-md-1">
                <div class="card rounded-0">
                    <div class="card-body">
                        <?php
                        if (!empty($login_message['error'])) {
                            foreach ($login_message['error'] as $mes){
                                echo '<div class="error text-center mx-auto">'.$mes.'</div>';
                            }
                        }
                        ?>
                        <form class="form" method="post" role="form" autocomplete="off" id="formLogin">
                            <div class="form-group" id="form-group" >
                            <span class="input input--hoshi">
					            <input class="input__field input__field--hoshi" type="text" id="input-2" name="username">
					            <label class="input__label input__label--hoshi" for="input-2">
						            <span class="input__label-content input__label-content--hoshi">Username</span>
					            </label>
				            </span>
                                <span class="input input--hoshi">
					            <input class="input__field input__field--hoshi" type="password" id="input-1" name="password">
					            <label class="input__label input__label--hoshi" for="input-1">
						            <span class="input__label-content input__label-content--hoshi">Password</span>
					            </label>
				            </span>
                            </div>
                            <button type="submit" class="btn rounded-0" value="in" name="in">Sign in</button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-6 login-img  order-1 order-md-2">
                <div class="block-vertical-center">
                    <div class="login-img-text text-center">
                        <h2>Sign in</h2>
                        <p>Parking System</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--REGISTRATION-->
<div class=" block-vertical-center" id="reg-bg">
    <div class="reg-container">
        <form method='post' class="form" role="form" autocomplete="off">
            <h3 class="text-center">Sign up</h3>
            <?php
                if (!empty($reg_message['error'])) {
                    foreach ($reg_message['error'] as $mes){
                        $mes = str_replace('_',' ',$mes);
                        echo '<div class="error text-center mx-auto">'.$mes.'</div>';
                    }
                }
            ?>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                            <span class="input input--hoshi">
					            <input class="input__field input__field--hoshi" type="text" id="input-5"
                                       name="username">
					            <label class="input__label input__label--hoshi" for="input-5">
						            <span class="input__label-content input__label-content--hoshi">Username</span>
					            </label>
				            </span>
                        <span class="input input--hoshi">
					            <input class="input__field input__field--hoshi" type="text" id="input-6"
                                       name="first_name">
					            <label class="input__label input__label--hoshi" for="input-6">
						            <span class="input__label-content input__label-content--hoshi">First Name</span>
					            </label>
				            </span>
                        <span class="input input--hoshi">
					            <input class="input__field input__field--hoshi" type="text" id="input-7"
                                       name="last_name">
					            <label class="input__label input__label--hoshi" for="input-7">
						            <span class="input__label-content input__label-content--hoshi">Last Name</span>
					            </label>
				            </span>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                            <span class="input input--hoshi">
					            <input class="input__field input__field--hoshi" type="text" id="input-8" name="email">
					            <label class="input__label input__label--hoshi" for="input-8">
						            <span class="input__label-content input__label-content--hoshi">Email</span>
					            </label>
				            </span>
                        <span class="input input--hoshi">
					            <input class="input__field input__field--hoshi" type="password" id="input-9"
                                       name="password">
					            <label class="input__label input__label--hoshi" for="input-9">
						            <span class="input__label-content input__label-content--hoshi">Password</span>
					            </label>
				            </span>
                        <span class="input input--hoshi">
					            <input class="input__field input__field--hoshi" type="password" id="input-10"
                                       name="confirm_password">
					            <label class="input__label input__label--hoshi" for="input-10">
						            <span class="input__label-content input__label-content--hoshi">Confirm Password</span>
					            </label>
				            </span>
                    </div>
                    <button type="submit" class="btn rounded-0 d-block mx-auto" name="up" value="up">Sign up</button>
                </div>
            </div>
        </form>
    </div>
</div>