<?php 
session_start();
include_once 'db_config.php';
include_once 'functions.php';

if (!empty($_POST['city_id'])) {
    $city_id = realEscape($_POST['city_id']);
    $routes = array();
    $schedules = array();
    $cities = array();
    $route_rates = array();
    $routes_temp = simpleSelect("SELECT route_id, from_city, to_city FROM routes WHERE from_city = '$city_id' OR to_city = '$city_id'");
    $routes_ids = array_column($routes_temp, 'route_id');
    $city_ids = array_unique(array_merge(array_column($routes_temp, 'from_city'), array_column($routes_temp, 'to_city')));
    if (!empty($routes_ids)) {
        $cities_temp = simpleSelect("SELECT city_id, city_name FROM cities WHERE city_id IN (" . implode(', ', $city_ids) . ")");
        foreach ($cities_temp as $city) {
            $cities[$city['city_id']] = $city['city_name'];
        }


        foreach ($routes_temp as $route) {
            if ($route['from_city'] == $city_id) {
                $routes['from'][] = $route;
            }
            else {
                $routes['to'][] = $route;
            }
        }

        $routes_ids_str = implode(', ', $routes_ids);
        $schedules_temp = simpleSelect("SELECT route_id, schedule_id, `day`, `start`, `end`, agency FROM schedules WHERE route_id IN (" . $routes_ids_str . ") ORDER BY start");
        $day_max_scheldules = array_fill_keys($days, 0);
        $day_max_scheldules_by_route = array_fill_keys($routes_ids, $day_max_scheldules);
        foreach ($schedules_temp as $value) {
            $schedules[$value['route_id']][$value['day']][] = $value;//$value['schedule_id']
            $day_max_scheldules_by_route[$value['route_id']][$value['day']] ++;
        }
        if (!empty($_SESSION['user'])) {
            $route_rates_temp = simpleSelect("SELECT * FROM route_rates WHERE user_id = '{$_SESSION['user']['user_id']}' AND route_id IN (" . $routes_ids_str . ")");
            foreach ($route_rates_temp as $ra) {
                $route_rates[$ra['route_id']] = $ra['rate'];
            }
        }
        $route_rates_avg_t = simpleSelect("SELECT route_id, AVG(rate) AS rate FROM route_rates WHERE route_id IN (" . $routes_ids_str . ") GROUP BY route_id");
        $route_rates_avg = array();
        foreach ($route_rates_avg_t as $ra) {
            $route_rates_avg[$ra['route_id']] = $ra['rate'];
        }
    }

}
else {
  die();
}

?>
<div class="col-4 mx-auto">
  <div class="list-group" id="list-tab" role="tablist">
    <?php 
    foreach ($routes as $route_type => $route) {
        echo '<h5>' . ucfirst($route_type) .  ' ' . $cities[$city_id] . '</h5>';
        foreach ($route as $r) {

            echo '<a class="list-group-item list-group-item-action" id="list-' . $r['route_id'] . '-list" data-toggle="list" href="#list-' . $r['route_id'] . '" role="tab" aria-controls="' . $r['route_id'] . '">' . $cities[$r['from_city']] . ' > ' . $cities[$r['to_city']] . ' </a>';
        }
    }
     ?>
    <!-- <a class="list-group-item list-group-item-action active" id="list-home-list" data-toggle="list" href="#list-home" role="tab" aria-controls="home">Home</a> -->
  
  </div>
</div>
<div class="col-8">
  <div class="tab-content" id="nav-tabContent">
    <?php 
        foreach ($routes_temp as $r) {
            $route_id = $r['route_id'];
            $max_row = max($day_max_scheldules_by_route[$route_id]);
            $sch_days = $schedules[$route_id] ?? array();
            echo '<div class="tab-pane fade" id="list-' . $route_id . '" role="tabpanel" aria-labelledby="list-' . $route_id . '">';
            echo '<h2 class="text-center">' . $cities[$r['from_city']] . ' > ' . $cities[$r['to_city']] . '</h2>';
            if (!empty($sch_days)) {
                if (!empty($_SESSION['user'])) {
                    echo '<div class="rating" data-route-id="' . $route_id . '" data-rate-value="' . ($route_rates[$route_id] ?? 0) . '"></div>' . ($route_rates_avg[$route_id] ?? '0.0');
                }
                echo '<table class="table">';
                echo '<thead class="thead-dark">';
                echo '<tr>';
                echo '<th>#</th>';
                foreach ($days as $day) {
                    echo '<th>' . ucfirst($day) . '</th>';
                }
                echo '</tr>';
                echo '</thead>';
                echo '<tbody>';
                echo "<pre>";
                for ($i = 0; $i < $max_row; $i++) { 
                    echo '<tr>';
                        echo '<td>' . ($i + 1) . '</td>';

                        foreach ($days as $one_day) {
                            $txt = '';
                            if (isset($sch_days[$one_day][$i])) {
                                $current_sch = $sch_days[$one_day][$i];
                                $txt = date('H:i', strtotime($current_sch['start'])) . ' > ' . date('H:i', strtotime($current_sch['end']));
                            }
                            echo '<td>' . $txt . '</td>';

                        }
                    echo '</tr>';
                }

                echo '</tbody>';

                echo '</table>';
            }
            else {
                echo '<div class="alert alert-primary">This table is empty</div>';
            }
            echo '</div>';

        }

    ?>
    <!-- <div class="tab-pane fade" id="list-settings" role="tabpanel" aria-labelledby="list-settings-list">..4.</div> -->
  </div>
</div>

