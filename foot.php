
  <!-- Footer -->
  <footer class="footer bg-light">
    <div class="container">
      <div class="row">
        <div class="col-lg-6 h-100 text-center text-lg-left my-auto">
          <ul class="list-inline mb-2">
            <li class="list-inline-item">
              <a href="login.php">Sign in</a>
            </li>
            <li class="list-inline-item">&sdot;</li>
            <li class="list-inline-item">
              <a href="registration.php">Sign up</a>
            </li>
            <li class="list-inline-item">&sdot;</li>
            <li class="list-inline-item">
              <a href="timetable.php">Timetable</a>
            </li>

          </ul>
          <p class="text-muted small mb-4 mb-lg-0">&copy; Fasttravel Website 2019. All Rights Reserved.</p>
        </div>
        <div class="col-lg-6 h-100 text-center text-lg-right my-auto">
          <ul class="list-inline mb-0">
            <li class="list-inline-item mr-3">
              <a href="#">
                <i class="fab fa-facebook fa-2x fa-fw"></i>
              </a>
            </li>
            <li class="list-inline-item mr-3">
              <a href="#">
                <i class="fab fa-twitter-square fa-2x fa-fw"></i>
              </a>
            </li>
            <li class="list-inline-item">
              <a href="#">
                <i class="fab fa-instagram fa-2x fa-fw"></i>
              </a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </footer>

  <!-- Bootstrap core JavaScript -->
  <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>
  <!-- <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script> -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
  <script src="js/rater.min.js"></script>
  <script type="text/javascript">
    <?php 
    $cities = simpleSelect("SELECT city_id as value, city_name as label FROM cities");
    $cities_2 = array_combine(array_column($cities, 'value'), array_column($cities, 'label'));
    echo 'var cities = ' . json_encode($cities) . ';';
    ?>
  </script>
  <script src="js/main.js"></script>
  <script type="text/javascript">
    $(function() {
      <?php 
      if (!empty($_GET['city_id']) && isset($cities_2[$_GET['city_id']])) {

        echo '$(".js-city-autocomplete").val("' . $cities_2[$_GET['city_id']] . '");';
        echo 'autocompleteSelect({}, {item:{value:' . $_GET['city_id'] . '}});';
      }
      ?>

    });
  </script>
</body>

</html>
