-- Adminer 4.6.2 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `cities`;
CREATE TABLE `cities` (
  `city_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `city_name` varchar(64) NOT NULL,
  PRIMARY KEY (`city_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `cities` (`city_id`, `city_name`) VALUES
(9,	'Szabadka'),
(10,	'Topolya'),
(11,	'Bajmok'),
(12,	'Orom');

DROP TABLE IF EXISTS `routes`;
CREATE TABLE `routes` (
  `route_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `from_city` int(10) unsigned NOT NULL,
  `to_city` int(10) unsigned NOT NULL,
  PRIMARY KEY (`route_id`),
  UNIQUE KEY `from_city_to_city` (`from_city`,`to_city`),
  KEY `from_city` (`from_city`),
  KEY `to_city` (`to_city`),
  CONSTRAINT `routes_ibfk_3` FOREIGN KEY (`from_city`) REFERENCES `cities` (`city_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `routes_ibfk_4` FOREIGN KEY (`to_city`) REFERENCES `cities` (`city_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `routes` (`route_id`, `from_city`, `to_city`) VALUES
(17,	9,	10),
(21,	9,	12),
(18,	10,	9),
(19,	10,	11),
(20,	11,	10),
(22,	12,	9);

DROP TABLE IF EXISTS `route_rates`;
CREATE TABLE `route_rates` (
  `route_rate_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `route_id` int(10) unsigned NOT NULL,
  `rate` tinyint(1) NOT NULL,
  PRIMARY KEY (`route_rate_id`),
  UNIQUE KEY `user_id_route_id` (`user_id`,`route_id`),
  KEY `route_id` (`route_id`),
  CONSTRAINT `route_rates_ibfk_3` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `route_rates_ibfk_4` FOREIGN KEY (`route_id`) REFERENCES `routes` (`route_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `route_rates` (`route_rate_id`, `user_id`, `route_id`, `rate`) VALUES
(7,	11,	17,	5),
(8,	13,	17,	2);

DROP TABLE IF EXISTS `schedules`;
CREATE TABLE `schedules` (
  `schedule_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `route_id` int(10) unsigned NOT NULL,
  `day` varchar(255) NOT NULL,
  `start` time NOT NULL,
  `end` time NOT NULL,
  `agency` varchar(255) NOT NULL,
  PRIMARY KEY (`schedule_id`),
  KEY `route_id` (`route_id`),
  CONSTRAINT `schedules_ibfk_2` FOREIGN KEY (`route_id`) REFERENCES `routes` (`route_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `schedules` (`schedule_id`, `route_id`, `day`, `start`, `end`, `agency`) VALUES
(11,	17,	'monday',	'11:11:00',	'14:22:00',	'Sutrans'),
(12,	17,	'monday',	'12:31:00',	'00:33:00',	'Sutrans'),
(13,	17,	'saturday',	'02:14:00',	'12:41:00',	'123'),
(14,	17,	'saturday',	'01:00:00',	'01:02:00',	'Sutrans'),
(15,	17,	'sunday',	'01:12:00',	'12:31:00',	'Sutrans');

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `user_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(256) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `users` (`user_id`, `name`, `email`, `password`, `status`) VALUES
(11,	'test',	'test1@test.com',	'219d57effa02b4a7326ccec9ed15fef546b0a6d4',	1),
(13,	'test2',	'test2@test.com',	'219d57effa02b4a7326ccec9ed15fef546b0a6d4',	0);

-- 2019-09-01 19:39:30
