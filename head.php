<?php
session_start();
include_once 'db_config.php';
include_once 'functions.php';

?>
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="This is a bus timetable page">
  <meta name="keywords" content="timetable, bus, school project">
  <meta name="author" content="Nagy Palóc Ákos">
  <meta name="robots" content="index, follow">

  <title>Fast travel</title>

  <!-- <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet"> -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" >

  <!-- <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet"> -->
  <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.9.0/css/all.min.css"/>
  <!-- <link href="vendor/simple-line-icons/css/simple-line-icons.css" rel="stylesheet" type="text/css"> -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/simple-line-icons/2.4.1/css/simple-line-icons.css" />
  <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css" />
  <link href="css/main.min.css" rel="stylesheet">

</head>

<body>

  <!-- Navigation -->
  <nav class="navbar navbar-light bg-light static-top">
    <div class="container">
      <a class="navbar-brand" href="./">Fast travel</a>
      <a class="btn btn-primary" href="timetable.php">Timetable</a>

      <?php 
      if (!empty($_SESSION['user'])) {
        echo '<a class="btn btn-primary" href="#">' . htmlspecialchars($_SESSION['user']['name']) . '</a>
            <a class="btn btn-primary" href="logout.php">Logout</a>';
      }
      else {
        echo '<a class="btn btn-primary" href="login.php">Sign In</a>
              <a class="btn btn-primary" href="registration.php">Sign Up</a>';
      }

      ?>
    </div>
  </nav>
