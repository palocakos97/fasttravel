<?php
require_once '../db_config.php';
require_once "../functions.php";
$response = array();

if (!empty($_GET['city'])) {
    $city_keyword = realEscape($_GET['city']);
    $routes_temp = simpleSelect("SELECT r.route_id, r.from_city, r.to_city 
        FROM routes r
        INNER JOIN cities c ON c.city_id = r.from_city OR c.city_id = r.to_city
        WHERE c.city_name LIKE '%{$city_keyword}%' ORDER BY c.city_name");
    // var_dump("SELECT r.route_id, r.from_city, r.to_city 
    //     FROM routes r
    //     INNER JOIN city c ON c.city_id = r.from_city OR c.city_id = r.to_city
    //     WHERE c.city_name LIKE '%{$city_keyword}%'");
    $routes_ids = array_column($routes_temp, 'route_id');
    $city_ids = array_unique(array_merge(array_column($routes_temp, 'from_city'), array_column($routes_temp, 'to_city')));

    $routes = array();
    if (!empty($routes_ids)) {
        $cities_temp = simpleSelect("SELECT city_id, city_name FROM cities WHERE city_id IN (" . implode(', ', $city_ids) . ")");
        foreach ($cities_temp as $city) {
            $cities[$city['city_id']] = $city['city_name'];
        }


        foreach ($routes_temp as $route) {
            $routes[] = $cities[$route['from_city']] . ' > ' . $cities[$route['to_city']];
        }
        $response = $routes;

    }
    else if (!isset($_GET['route_num']) && empty($response)) {
        $response = ["No results"];
    }

    if (!empty($_GET['route_num']) ) {
        if (!empty($routes_temp[($_GET['route_num'] - 1)])) {
            $current_route = $routes_temp[($_GET['route_num'] - 1)];
            $schedules_temp = simpleSelect("SELECT route_id, schedule_id, `day`, `start`, `end`, agency FROM schedules WHERE route_id = '{$current_route['route_id']}' ORDER BY start");
            $schedules = array();
            foreach ($schedules_temp as $value) {
                $schedules[$value['day']][] = $value['start'] . ' > ' . $value['end'];//$value['schedule_id']
            }
            $schedules_for_api = array();
            foreach ($schedules as $day_name => $schs) {
                $schedules_for_api[] = strtoupper($day_name);
                foreach ($schs as $time) {
                    $schedules_for_api[] = $time;
                }
            }
            $response = $schedules_for_api;
            if (empty($response)) {
                $response = ["This table is empty!"];
            }
        }
        else {
            $response = ["Error, not valid route number"];
        }


    }
}


echo json_encode($response);
?>
