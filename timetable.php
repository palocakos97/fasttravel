<?php
include_once 'head.php';


?>
<body>
  <div class="container">
    <div class="row">
      <div class="col-md-4"></div>
      <div class="col-md-4 text-center my-5">
        <h1 class=" ">Choose a city</h1>
        <input class="form-control js-city-autocomplete" >
      </div>
      <div class="col-md-4"></div>
    </div>
    <div class="row js-timetable">
      <!-- ajax -->
    </div>
  </div>
</body>

<?php include_once 'foot.php'; ?>