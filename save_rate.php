<?php 
session_start();
include_once 'db_config.php';

include_once 'functions.php';

if (!empty($_POST['route_id']) && !empty($_POST['value']) && isset($_SESSION['user'])) {
	$data['user_id'] = $_SESSION['user']['user_id'];
	$data['rate'] = (int)realEscape($_POST['value']);
	$data['route_id'] = (int)realEscape($_POST['route_id']);
	insertOrUpdate($data, 'route_rates');
}
?>