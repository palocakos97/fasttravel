<?php


function paramSelect($columns, $table, $where = '1') {
    global $connection;
    $data = array();
    $sql = "SELECT " . implode(', ', $columns) . " FROM " . $table . " WHERE " . $where;

    $result = mysqli_query($connection, $sql);
    while ($record = mysqli_fetch_assoc($result)) {
        $data[] = $record;
    }
    mysqli_free_result($result);
    return $data;
}


function simpleSelect($sql) {
    global $connection;
    $data = array();
    $result = mysqli_query($connection, $sql);
    while ($record = mysqli_fetch_assoc($result)) {
        $data[] = $record;
    }
    mysqli_free_result($result);
    return $data;
}

function query($sql) {
    global $connection;

    return mysqli_query($connection, $sql);
}


function insertOrUpdate($data, $table, $where = '') {
    global $connection;
    $sql_array = array();
    foreach ($data as $key => $value) {
        $sql_array[] = "`$key` = '$value'";
    }
    if ($where === '') {
        $prefix = "INSERT INTO";
    }
    else {
        $prefix = "UPDATE";
        $where = " WHERE " . $where;
    }
    $sql = $prefix . " $table SET " . implode(', ', $sql_array) . $where;
    var_dump($sql);
    return mysqli_query($connection, $sql);
}

function checkUploadAndInsert($table, $data, $update, $key) {
    $param = [$data, $table];
    $flag = false;
    $message = ['error' => array(), 'success' => ''];
    if ($update) {
        $param[] = $key . ' = ' . realEscape($_GET['Update']);
    }
    if (call_user_func_array('insertOrUpdate', $param)) {
        $message['success'] = 'Save successful';
        $flag = true;
    }
    else {
        $message['error'][] = 'Something Wrong, try it later!';
    }
    return ['message' => $message, 'flag' => $flag];
}

function realEscape($data) {
    global $connection;
    if (is_array($data)) {
        foreach ($data as $key => $item) {
            $data[$key] = mysqli_real_escape_string($connection, trim($item));
        }
    }
    else {
        $data = mysqli_real_escape_string($connection, trim($data));
    }
    return $data;
}


function checkRequire($columns, $update) {
    $message = ['error' => array(), 'success' => ''];
    foreach ($columns as $item) {
        if ((!isset($_POST[$item]) || strlen(trim($_POST[$item])) == 0) && !($update && $item == 'password')) {
            $message['error'][] = 'Required to fill the ' . $item . '.';
        }
    }
    return $message;
}


function dataFiltering($data, $extension = ['']) {
    $filtered_data = array();
    $extension = array_flip($extension);
    foreach ($data as $key => $value) {
        if (!isset($extension[$key])) {
            $filtered_data[realEscape($key)] = realEscape($_POST[$key]);
        }
    }
    return $filtered_data;
}


function checkExistingInDatabase($table, $columns, $update, $data) {
//    var_dump($table,$columns,$update,$data);
    $condition = '';
    $message = array();
    if ($update) {
        $condition = "AND `" . $columns['info']['primarykey'][0] . "` != '" . realEscape($_GET['Update']) . "'";
    }
    $uniq_data = array();
    foreach ($columns['info']['unique'] as $uniq_field) {
        $uniq_data[$uniq_field] = $data[$uniq_field];
    }
    $uniq_check = existInDatabase($uniq_data, $table, $condition);
    foreach ($uniq_check as $uniq) {
        $message['error'][] = 'The ' . $uniq . ' alredy exist!';
    }
    return $message;
}
