<?php
include_once 'head.php';
if (isset($_POST['up'])) {
    $required_columns = ['name', 'password','confirm_password', 'email'];
    $reg_message = checkRequire($required_columns, false);
    if (empty($reg_message['error'])) {
        if ($_POST['password'] != $_POST['confirm_password']) {
            $reg_message['error'][] = 'The passwords do not match!';
        }
        if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
            $reg_message['error'][] = 'Invalid email!';
        }
        $exceptions = ['up','confirm_password'];
        $data = dataFiltering($_POST, $exceptions);
        if (!empty(simpleSelect("SELECT user_id FROM users WHERE email = '{$data['email']}'"))) {
          $reg_message['error'][] = 'This email is already in use';
        }

        if (empty($reg_message['error'])) {
            $data['password'] = sha1(SALT . $data['password']);
            $result = checkUploadAndInsert('users', $data, false, 'user_id');
            if ($result['flag']) {
              header('location: login.php?reg_success');
            }
            else {
              $reg_message['error'] = $result['message']['error'];
            }
        }
    }
}
?>
<body>
  <div class="container">
    <div class="row">
      <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
        <div class="card card-signin my-5">
          <div class="card-body">
            <h5 class="card-title text-center">Sign In</h5>
            <?php

            if (!empty($reg_message['error'])) {
                foreach ($reg_message['error'] as $mes){
                    echo '<div class="alert alert-danger text-center mx-auto">' . $mes . '</div>';
                }
            }
            ?>
            <form class="form-signin" method="POST">
              <div class="form-label-group">
                <input name="name" type="text" id="inputEmail" class="form-control" placeholder="Name" value="<?php echo $_POST['name'] ?? '';?>" required autofocus><br/>
              </div>

              <div class="form-label-group">
                <input name="email" type="email" id="inputEmail" class="form-control" placeholder="Email address" value="<?php echo $_POST['email'] ?? '';?>" required><br/>
              </div>

              <div class="form-label-group">
                <input name="password" type="password" id="inputPassword" class="form-control" placeholder="Password" required><br/>
              </div>

              <div class="form-label-group">
                <input name="confirm_password" type="password" class="form-control" placeholder="Password confirm" required><br/>
              </div>


   

              <button class="btn btn-lg btn-primary btn-block text-uppercase" type="submit" name="up">Sign in</button>
              <hr class="my-4">
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</body>

<?php include_once 'foot.php'; ?>