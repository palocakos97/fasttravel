<?php
include_once 'head.php';
?>

  <!-- Masthead -->
  <header class="masthead text-white text-center">
    <div class="overlay"></div>
    <div class="container">
      <div class="row">
        <div class="col-xl-9 mx-auto">
          <h1 class="mb-5">Find the bus timetable for your city!</h1>
        </div>
        <div class="col-md-10 col-lg-8 col-xl-7 mx-auto">
          <form>
            <div class="form-row">
              <div class="col-12 col-md-9 mb-2 mb-md-0 mx-auto">
                <input type="text" class="form-control form-control-lg js-city-autocomplete" placeholder="Enter a city ..." >
              </div>

            </div>
          </form>
        </div>
      </div>
    </div>
  </header>


  <!-- Image Showcases -->
  <section class="showcase">
    <div class="container-fluid p-0">
      <div class="row no-gutters">

        <div class="col-lg-6 order-lg-2 text-white showcase-img" style="background-image: url('img/bus1.jpg');"></div>
        <div class="col-lg-6 order-lg-1 my-auto showcase-text">
          <h2>Lorem ipsum dolor sit amet</h2>
          <p class="lead mb-0">Aliquam lacus urna, finibus bibendum dapibus nec, vulputate nec velit. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Nulla id velit in lorem faucibus egestas. Suspendisse laoreet aliquet elit quis finibus. Suspendisse potenti.</p>
        </div>
      </div>
      <div class="row no-gutters">
        <div class="col-lg-6 text-white showcase-img" style="background-image: url('img/bus2.jpg');"></div>
        <div class="col-lg-6 my-auto showcase-text">
          <h2>Lorem ipsum dolor sit amet</h2>
          <p class="lead mb-0">Maecenas lacinia odio in feugiat venenatis. In hac habitasse platea dictumst. Fusce convallis commodo pharetra. Quisque mollis diam massa. Duis dictum leo vestibulum orci placerat consectetur. </p>
        </div>
      </div>
      <div class="row no-gutters">
        <div class="col-lg-6 order-lg-2 text-white showcase-img" style="background-image: url('img/bus3.jpg');"></div>
        <div class="col-lg-6 order-lg-1 my-auto showcase-text">
          <h2>Lorem ipsum dolor sit amet</h2>
          <p class="lead mb-0">Nullam ut nunc id erat elementum mollis non hendrerit orci. Mauris in dignissim arcu, sit amet placerat magna. Ut vitae mollis nibh. Morbi maximus euismod odio, nec facilisis purus ultrices vitae. Phasellus a libero vel justo volutpat varius nec vel nulla. Vestibulum eget consectetur quam, non porttitor magna.</p>
        </div>
      </div>
    </div>
  </section>

  <!-- Testimonials -->
  <section class="testimonials text-center bg-light">
    <div class="container">
      <h2 class="mb-5">The creator...</h2>
      <div class="row">
        <div class="col-lg-12">
          <div class="testimonial-item mx-auto mb-5 mb-lg-0">
            <img class="img-fluid rounded-circle mb-3" src="img/paloc.jpg" alt="">
            <h5>Nagy Palóc Ákos</h5>
            <p class="font-weight-light mb-0">Manager</p>
          </div>
        </div>
      </div>
    </div>
  </section>


<?php include_once 'foot.php'; ?>